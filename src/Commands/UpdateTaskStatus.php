<?php 

namespace Sirs\Tasks\Commands;

use Illuminate\Support\Facades\Bus;
use Sirs\Tasks\Interfaces\Task;
use Sirs\Tasks\Interfaces\TaskStatus;

class UpdateTaskStatus extends Command
{
  public $task;

  public $newStatus;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task, TaskStatus $newStatus)
  {
    $this->task = $task;
    $this->newStatus = $newStatus;
  }

  /**
   * update the task's status and save
   * DEPRECATED
   *
   * @author
   **/
  public function handle(): void
  {
    if (is_null($this->task->date_started)) {
      Bus::dispatch(new StartTask($this->task));
    }
    $this->task->task_status_id = $this->newStatus->id;
    $this->task->save();
  }
}
