<?php 

namespace Sirs\Tasks\Commands;

use Sirs\Tasks\Interfaces\Task;

class DeleteTask extends Command
{
  public $task;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }

  /**
   * DEPRECATED
   *
   *
   * @author
   **/
  public function handle(): void
  {
    $this->task->delete();
  }
}
