<?php 

namespace Sirs\Tasks\Commands;

use Illuminate\Support\Facades\Bus;
use Sirs\Tasks\Interfaces\Task;

class CompleteTask extends Command
{
  public $task;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }

  /**
   * undocumented function
   *
   *
   * @author
   **/
  public function handle(): void
  {
    Bus::dispatch(new UpdateTaskStatus($this->task, class_taskStatus()::findByName('completed')));
  }
}
