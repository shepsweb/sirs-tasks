<?php 

namespace Sirs\Tasks\Commands;

use Carbon\Carbon;
use Sirs\Tasks\Interfaces\Task;

class StartTask extends Command
{
  public $task;

  public $dateStarted;

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct(Task $task, Carbon $date_started = null)
  {
    $this->task = $task;
    $this->dateStarted = ($date_started) ? $date_started : new Carbon();
  }

  /**
   * undocumented function
   *
   *
   * @author
   **/
  public function handle(): void
  {
    $this->task->startTask($this->dateStarted);
  }
}
