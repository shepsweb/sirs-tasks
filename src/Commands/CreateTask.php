<?php 

namespace Sirs\Tasks\Commands;

class CreateTask extends Command
{
  public $task_type_id;

  public $owner_type;

  public $owner_id;

  public $date_due;

  /**
   * Create a new command instance.
   * DEPRECATED
   *
   * @return void
   */
  public function __construct($task_type_id, $owner_type, $owner_id, $date_due)
  {
    $this->task_type_id = $task_type_id;
    $this->owner_type = $owner_type;
    $this->owner_id = $owner_id;
    $this->date_due = $date_due;
  }

  /**
   * undocumented function
   *
   *
   * @author
   **/
  public function handle(): void
  {
    class_task()::create([
        'task_type_id' => $this->task_type_id,
        'owner_type' => $this->owner_type,
        'owner_id' => $this->owner_id,
        'date_due' => $this->date_due,
    ]);
  }
}
