<?php 

namespace Sirs\Tasks\Commands;

use Illuminate\Support\Facades\Event;
use Sirs\Tasks\Events\TaskChildrenCreated;
use Sirs\Tasks\Interfaces\Task;

class CreateTaskChildren extends Command
{
    public $task;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Task $task)
    {
    $this->task = $task;
    }

    /**
     * undocumented function
     *
     *
     * @author
     **/
    public function handle(): void
    {
        if ($this->task->taskType->has_children) {
            foreach ($this->task->taskType->children as $childTaskType) {
                $t = class_task()::create([
                    'task_type_id' => $childTaskType->id,
                    'owner_type' => $this->task->owner_type,
                    'owner_id' => $this->task->owner_id,
                    'parent_task_id' => $this->task->id,
                    'date_due' => $this->task->date_due,
                ]);
            }
            Event::dispatch(new TaskChildrenCreated($this->task));
        }
    }
}
