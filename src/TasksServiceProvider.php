<?php

namespace Sirs\Tasks;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Event;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Sirs\Tasks\Console\CreateTaskTypeWorkflowStrategy;
use Sirs\Tasks\Events\TaskAutoCanceled;
use Sirs\Tasks\Events\TaskCanceled;
use Sirs\Tasks\Events\TaskChildrenCreated;
use Sirs\Tasks\Events\TaskCompleted;
use Sirs\Tasks\Events\TaskCreated;
use Sirs\Tasks\Events\TaskDataUpdated;
use Sirs\Tasks\Events\TaskDeleted;
use Sirs\Tasks\Events\TaskFailed;
use Sirs\Tasks\Events\TaskMissed;
use Sirs\Tasks\Events\TaskReOpened;
use Sirs\Tasks\Events\TaskStarted;
use Sirs\Tasks\Events\TaskStatusUpdated;
use Sirs\Tasks\Handlers\Events\RunTaskWorkflow;
use Sirs\Tasks\Handlers\Events\StartParentTask;
use Sirs\Tasks\Handlers\Events\TaskWasCreatedHandler;

class TasksServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the Tasks services.
     * Adds listener for Task::created, Task::updated, and TaskStatusWasUpdated Events
     */
    public function boot(Router $router): void
    {
        $this->bindInterfaces();

        $this->registerListeners();

        // load the views
        $this->loadViewsFrom(__DIR__.'/views', 'tasks');

        // Publish the config files
        $this->publish();

        // register the routes
        include __DIR__.'/routes.php';

        // bind the task model to the task routes
        $router->model('api/tasks', class_task());
    }

    /**
     * Register the application services.
     */
    public function register(): void
    {
        $this->commands([
            CreateTaskTypeWorkflowStrategy::class,
        ]);
    }

    protected function bindInterfaces()
    {
        $this->app->bind(
            \Sirs\Tasks\Task::class,
            config('tasks.bindings.models.Task', \Sirs\Tasks\Task::class)
        );
        $this->app->bind(
            \Sirs\Tasks\TaskType::class,
            config('tasks.bindings.models.TaskType', \Sirs\Tasks\TaskType::class)
        );
        $this->app->bind(
            \Sirs\Tasks\TaskStatus::class,
            config('tasks.bindings.models.TaskStatus', \Sirs\Tasks\TaskStatus::class)
        );

        $this->app->bind(
            \Sirs\Tasks\Interfaces\Task::class,
            config('tasks.bindings.models.Task', \Sirs\Tasks\Task::class)
        );
        $this->app->bind(
            \Sirs\Tasks\Interfaces\TaskType::class,
            config('tasks.bindings.models.TaskType', \Sirs\Tasks\TaskType::class)
        );
        $this->app->bind(
            \Sirs\Tasks\Interfaces\TaskStatus::class,
            config('tasks.bindings.models.TaskStatus', \Sirs\Tasks\TaskStatus::class)
        );
    }

    protected function publish()
    {
        // Publish the triggers class
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('tasks.php'),
        ], 'task_config');

        // Publish the migrations
        $this->publishes([
            __DIR__.'/database/migrations/' => database_path('/migrations'),
        ], 'task_migrations');

        // Publish the seeds
        $this->publishes([
            __DIR__.'/database/seeds/' => database_path('/seeders'),
        ], 'task_seeds');

        // Publish the factories
        $this->publishes([
            __DIR__.'/database/factories/' => database_path('/factories'),
        ], 'task_factories');

        $this->publishes([
            __DIR__.'/TaskRouterDefault.php' => app_path('TaskRouter.php'),
        ], 'task_router');
    }

    protected function registerListeners()
    {
        $this->registerWorkflowEvents();
        Event::listen(TaskCreated::class, TaskWasCreatedHandler::class);

        // Event::listen(Sirs\Tasks\Events\TaskStatusUpdated::class, Sirs\Tasks\Handlers\Events\TaskStatusWasUpdatedHandler::class);
        
        Event::listen(TaskStarted::class, StartParentTask::class);

        // register observers:
        class_task()::observe(new TaskObserver);
    }

    protected function registerWorkflowEvents()
    {
        // Register RunTaskWorkflow listener
        // There has to be a better way to do this, but I can't find it.
        Event::listen(TaskAutoCanceled::class, RunTaskWorkflow::class);
        Event::listen(TaskCanceled::class, RunTaskWorkflow::class);
        Event::listen(TaskChildrenCreated::class, RunTaskWorkflow::class);
        Event::listen(TaskCompleted::class, RunTaskWorkflow::class);
        Event::listen(TaskCreated::class, RunTaskWorkflow::class);
        Event::listen(TaskDataUpdated::class, RunTaskWorkflow::class);
        Event::listen(TaskDeleted::class, RunTaskWorkflow::class);
        Event::listen(TaskFailed::class, RunTaskWorkflow::class);
        Event::listen(TaskMissed::class, RunTaskWorkflow::class);
        Event::listen(TaskReOpened::class, RunTaskWorkflow::class);
        Event::listen(TaskStarted::class, RunTaskWorkflow::class);
        Event::listen(TaskStatusUpdated::class, RunTaskWorkflow::class);
    }
}
