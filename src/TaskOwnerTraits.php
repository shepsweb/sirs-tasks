<?php 

namespace Sirs\Tasks;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait TaskOwnerTraits
{
  public function tasks(): MorphMany
  {
    return $this->morphMany(Sirs\Tasks\Task::class, 'owner');
  }

  protected function tasksWithStatus($taskStatusId)
  {
    return $this->tasks()->where('task_status_id', '=', $taskStatusId);
  }

  protected function getTasksWithStatus($taskStatusId)
  {
    return $this->tasksWithStatus($taskStatusId)->get();
  }

  public function getPendingTasks()
  {
    return $this->tasksWithStatus(class_taskStatus()::findByName('Pending')->id)->orWhere('task_status_id', '=', null)->get();
  }

  public function getCompletedTasks()
  {
    return $this->getTasksWithStatus(class_taskStatus()::findByName('Complete')->id);
  }

  public function getMissedTasks()
  {
    return $this->getTasksWithStatus(class_taskStatus()::findByName('Missed')->id);
  }

  public function getFailedTasks()
  {
    return $this->getTasksWithStatus(class_taskStatus()::findByName('Failed')->id);
  }

  public function getCanceledTasks()
  {
    return $this->getTasksWithStatus(class_taskStatus()::findByName('Cancelled')->id);
  }

  public function getAutocanceledTasks()
  {
    return $this->getTasksWithStatus(class_taskStatus()::findByName('Autocancelled')->id);
  }

  /**
   * Cancels all pending tasks for this task owner
   *
   * @return Collection collection of canceled tasks
   */
  public function cancelPendingTasks()
  {
    $canceledTasks = new Collection();
    foreach ($this->getPendingTasks() as $task) {
      $task->task_status_id = class_taskStatus()::findByName('Cancelled')->id;
      $task->save();
      $canceledTasks->push($task);
    }

    return $canceledTasks;
  }
}
