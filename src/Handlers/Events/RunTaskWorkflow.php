<?php 

namespace Sirs\Tasks\Handlers\Events;

use Sirs\Tasks\Events\TaskEvent;

class RunTaskWorkflow
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TaskEvent $event): void
    {
        $task = $event->task->fresh();

        $workflowClassName = $task->taskType->getWorkflowClass();
        if (class_exists($workflowClassName)) {
            $workflow = new $workflowClassName($task, $event);
            $workflow->run();
        }
    }
}
