<?php 

namespace Sirs\Tasks\Handlers\Events;

use Illuminate\Support\Facades\Bus;
use Sirs\Tasks\Commands\StartTask;
use Sirs\Tasks\Events\TaskStarted;

class StartParentTask
{
  /**
   * Create the event handler.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   */
  public function handle(TaskStarted $event): void
  {
    $task = $event->task;

    if ($task->parent) {
      Bus::dispatch(new StartTask($task->parent));
    }
  }
}
