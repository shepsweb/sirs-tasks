<?php 

namespace Sirs\Tasks\Handlers\Events;

use Illuminate\Support\Facades\Bus;
use Sirs\Tasks\Commands\CreateTaskChildren;
use Sirs\Tasks\Events\TaskCreated;

class TaskWasCreatedHandler
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(TaskCreated $event): void
    {
        Bus::dispatch(new CreateTaskChildren($event->task));
    }
}
