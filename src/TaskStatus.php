<?php

namespace Sirs\Tasks;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Sirs\Tasks\Interfaces\TaskStatus as TaskStatusInterface;

class TaskStatus extends Model implements TaskStatusInterface
{
    use Sluggable;
    use SluggableScopeHelpers;
  
    protected $table = 'task_statuses';
    
    protected $fillable = ['name'];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    public static function findByName($name)
    {
        return static::where('name', '=', $name)
            ->get()
            ->first();
    }
}
