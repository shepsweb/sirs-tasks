<?php

$routeGroup = (config('tasks.routeGroup')) ? config('tasks.routeGroup') : [];
Route::group($routeGroup, function () {
    Route::group(['prefix'=>config('tasks.apiPrefix')], function () {
        Route::resource('tasks', \Sirs\Tasks\Controllers\RestTaskController::class, ['as' => 'tasks_api']);
        Route::patch('tasks/{id}/complete', [\Sirs\Tasks\Controllers\RestTaskController::class, 'complete'])->name('completeTask');
        Route::patch('tasks/{id}/fail', [\Sirs\Tasks\Controllers\RestTaskController::class, 'fail'])->name('failTask');
        Route::get('task-types', [\Sirs\Tasks\Controllers\RestTaskController::class, 'types'])->name('taskTypes');
        Route::get('task-statuses', [\Sirs\Tasks\Controllers\RestTaskController::class, 'statuses'])->name('taskStatuses');
    });

    Route::name('tasks.')->group(function () {
        Route::resource('tasks/admin', \Sirs\Tasks\Controllers\TaskController::class);
    });

    Route::get('tasks/{id}', [\Sirs\Tasks\Controllers\TaskController::class, 'defaultShow'])->name('tasks.show');
    Route::put('tasks/{id}', [\Sirs\Tasks\Controllers\TaskController::class, 'defaultUpdate'])->name('tasks.update');

    // Route::get('workflow/{id}', ['as'=>'showWorkflow', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@showWorkflow']);
  // Route::post('workflow/{id}', ['as'=>'updateWorkflow', 'uses'=>'\Sirs\Tasks\Controllers\TaskController@saveWorkflow']);
});
