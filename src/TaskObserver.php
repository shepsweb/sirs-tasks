<?php

namespace Sirs\Tasks;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Sirs\Tasks\AppointmnentType;
use Sirs\Tasks\Events\TaskAutoCanceled;
use Sirs\Tasks\Events\TaskCanceled;
use Sirs\Tasks\Events\TaskCompleted;
use Sirs\Tasks\Events\TaskCreated;
use Sirs\Tasks\Events\TaskDataUpdated;
use Sirs\Tasks\Events\TaskDeleted;
use Sirs\Tasks\Events\TaskFailed;
use Sirs\Tasks\Events\TaskMissed;
use Sirs\Tasks\Events\TaskReOpened;
use Sirs\Tasks\Events\TaskStarted;
use Sirs\Tasks\Events\TaskStatusUpdated;
use Sirs\Tasks\Events\TaskStatusWasUpdated;
use Sirs\Tasks\Events\TaskWasCreated;
use Sirs\Tasks\Interfaces\Task;
use Sirs\Tasks\Interfaces\TaskStatus;

/**
 * participant model observer
 *
 * @author
 **/
class TaskObserver
{
    use \Illuminate\Foundation\Bus\DispatchesJobs;

    public $eventsDispatcher;

    public function __construct()
    {
    }

    public function saving(Task $task)
    {
        // get the current user and set the updated_by_type and updated_by_id
        $user = Auth::user(); // this probably shouldn't be here b/c, theoretically some other model could update a task.
        if ($user) {
            $task->updated_by_type = $user::class;
            $task->updated_by_id = $user->id;
        }

        if (! $task->owner_type) {
            throw new InvalidOwnerType(null);
        }
    }

    public function created(Task $task)
    {
        Event::dispatch(new TaskWasCreated($task));
        Event::dispatch(new TaskCreated($task));
    }

    public function updated(Task $task)
    {
        if ($task->isDirty('task_status_id')) {
            Event::dispatch(new TaskStatusWasUpdated($task));
            Event::dispatch(new TaskStatusUpdated($task));
            switch ($task->task_status_id) {
                case 1: // changed back to pending
                    Event::dispatch(new TaskReOpened($task));
                    break;
                case 2: // changed to complete
                    Event::dispatch(new TaskCompleted($task));
                    break;
                case 3: // changed to Missed
                    Event::dispatch(new TaskMissed($task));
                    break;
                case 4: // changed to failed
                    Event::dispatch(new TaskFailed($task));
                    break;
                case 5: // changed to canceled
                    Event::dispatch(new TaskCanceled($task));
                    break;
                case 6: // changed to autocanceled
                    Event::dispatch(new TaskAutoCanceled($task));
                    break;
                default:
                    break;
            }
        }

        if ($task->isDirty('date_started') && ! is_null($task->date_started)) {
            Event::dispatch(new TaskStarted($task));
        }

        if ($task->isDirty('data')) {
            Event::dispatch(new TaskDataUpdated($task));
        }
    }

    public function deleted(Task $task)
    {
        Event::dispatch(new TaskDeleted($task));
    }
} // END class ParticipantObserver
