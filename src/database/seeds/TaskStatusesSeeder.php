<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class TaskStatusesSeeder extends Seeder
{
  /**
   * Run the database seeds.
   */
  public function run(): void
  {
    Model::unguard();

    class_taskStatus()::create(['id' => '1',
        'name' => 'Pending',
        'slug' => 'pending',
    ]);
    class_taskStatus()::create(['id' => '2',
        'name' => 'Completed',
        'slug' => 'completed',
    ]);
    class_taskStatus()::create(['id' => '3',
        'name' => 'Missed',
        'slug' => 'missed',
    ]);
    class_taskStatus()::create(['id' => '4',
        'name' => 'Failed',
        'slug' => 'failed',
    ]);
    class_taskStatus()::create(['id' => '5',
        'name' => 'Cancelled',
        'slug' => 'cancelled',
    ]);
    class_taskStatus()::create(['id' => '6',
        'name' => 'Autocancelled',
        'slug' => 'autocancelled',
    ]);
  }
}
