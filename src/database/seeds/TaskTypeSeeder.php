<?php

namespace Database\Seeders;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;
use Sirs\Tasks\TaskType;

class TaskTypeSeeder extends Seeder
{
  /**
   * Run the database seeds.
   */
  public function run(): void
  {
    Model::unguard();
    $taskTypes = collect(config('tasks.types'));
    if ($taskTypes->count() > 0) {
      foreach ($taskTypes as $type) {
        TaskType::create($type);
      }
    }
  }
}
