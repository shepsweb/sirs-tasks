<?php

namespace Database\Factories;

use App\Models\Participant;
use Illuminate\Database\Eloquent\Factories\Factory;

class TaskFactory extends Factory
{
    protected $model = Task::class;

    /**
     * Define the model's default state.
     */
    public function definition(): array
    {
        $part = Participant::all()->random();
        $taskType = class_taskType()::inRandomOrder()->first();

        return [
            'task_type_id' => $taskType->id,
            'owner_type' => $part::class,
            'owner_id' => $part->id,
            'date_due' => $this->faker->dateTimeBetween('-10 days', '+14 days'),
            'task_status_id' => 1,
        ];
    }
}
