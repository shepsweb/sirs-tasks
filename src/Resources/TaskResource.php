<?php

namespace Sirs\Tasks\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class TaskResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     */
    public function toArray(Request $request): array
    {
        $data = parent::toArray($request);
        $data['date_started'] = $this->date_due->format('Y-m-d');
        $data['date_due'] = $this->date_due->format('Y-m-d');
        $data['owner'] = $this->whenLoaded('owner');
        $data['task_type'] = new TaskTypeResource($this->whenLoaded('taskType'));
        $data['task_status'] = new TaskStatusResource($this->whenLoaded('taskStatus'));
        $data['child_order'] = ($this->relationLoaded('taskType')) ? $this->taskType->child_order : null;
        $data['children'] = TaskResource::collection($this->whenLoaded('children'));
        if (
            $this->relationLoaded('owner')
            && isset(config('tasks.owner_resources')[$this->owner::class])
        ) {
            $resourceClass = config('tasks.owner_resources')[$this->owner::class];
            $data['owner'] = new $resourceClass($this->whenLoaded('owner'));
        }

        return $data;
    }
}
