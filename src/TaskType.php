<?php

namespace Sirs\Tasks;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;
use Sirs\Tasks\Interfaces\TaskType as TaskTypeInterface;

class TaskType extends Model implements TaskTypeInterface
{
    use Sluggable;
    use SluggableScopeHelpers;

    protected $table = 'task_types';
    
    protected $fillable = [
        'id',
        'name',
        'description',
        'has_children',
        'parent_task_type_id',
        'child_order',
        'display_order',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    public function children(): HasMany
    {
        return $this->hasMany(
            config('tasks.bindings.models.TaskType', class_taskType()),
            'parent_task_type_id',
            'id'
        );
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(
            config('tasks.bindings.models.TaskType', class_taskType()),
            'parent_task_type_id',
            'id'
        );
    }

    public function siblings(): BelongsTo
    {
        return $this->belongsTo(config('tasks.bindings.models.TaskType', class_taskType()))
            ->where('parent_task_type_id', '=', $this->parent_task_type_id)
            ->where('id', '!=', $this->id);
    }

    public function tasks(): HasMany
    {
        return $this->hasMany(\Sirs\Tasks\Task::class);
    }

    public function siblingsQuery()
    {
        $query = static::where('parent_task_type_id', $this->parent_task_type_id)
            ->where('id', '!=', $this->id);

        return $query;
    }

    public function getSiblings()
    {
        return static::where('parent_task_type_id', $this->parent_task_type_id)
            ->where('id', '!=', $this->id)
            ->orderby('child_order')
            ->get();
    }

    public function getWorkflowClass()
    {
        return $this->getSafeWorkflowClassName();
    }

    public function getSafeWorkflowClassName()
    {
        $numerics = '';
        $slug = $this->slug;
        while (is_numeric(substr($slug, 0, 1))) {
            $numerics .= substr($slug, 0, 1);
            $slug = substr($slug, 1);
        }
        if ($numerics !== '') {
            $numFormatter = new \NumberFormatter('en', \NumberFormatter::SPELLOUT);
            $slug = $numFormatter->format($numerics).$slug;
        }

        return 'App\\Tasks\\'.ucfirst(Str::camel($slug).'WorkflowStrategy');
    }

    public static function findBySlug($slug)
    {
        return static::where('slug', $slug)
            ->get()
            ->first();
    }
}
