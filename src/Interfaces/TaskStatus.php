<?php

namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task statuses must adhere to.
 *
 * @author
 **/
interface TaskStatus
{
    public function sluggable();

    public static function findByName($name);
}
