<?php

namespace Sirs\Tasks\Interfaces;

/**
 * Defines interface all task owners must adhere to.
 *
 * @author
 **/
interface TaskOwner
{
  public function fullName();

  public function tasks();

  public function getPendingTasks();

  public function getCompletedTasks();

  public function getMissedTasks();

  public function getFailedTasks();

  public function getCanceledTasks();

  public function cancelPendingTasks();
} // END interface OwnerInterface