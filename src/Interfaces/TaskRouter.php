<?php

namespace Sirs\Tasks\Interfaces;

/**
 * TaskRouters get the url for a task based on its type and project specific rules
 *
 * @author
 **/
interface TaskRouter
{
  /**
   * Gets the url if custom routing
   *
   * @return mixed url or null if default
   **/
  public function resolve(Task $task);
} // END interface TaskRouter
