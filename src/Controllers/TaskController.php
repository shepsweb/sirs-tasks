<?php

namespace Sirs\Tasks\Controllers;

use Carbon\Carbon;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): Response
    {
        $search_text = $request->search_text;
        $taskQuery = class_task()::with('owner', 'taskStatus', 'taskType');
        if ($search_text) {
            $taskQuery->where('tasks.id', '=', $search_text)->orWhere('owner_id', '=', $search_text);
            $taskQuery->orWhereHas('taskType', function ($query) use ($search_text) {
                $query->where('name', 'LIKE', '%'.$search_text.'%');
            });
            $taskQuery->orWhereHas('taskStatus', function ($query) use ($search_text) {
                $query->where('name', 'LIKE', '%'.$search_text.'%');
            });
        }
        $tasks = $taskQuery->paginate(10);
        $tasks->appends(['search_text' => $search_text]);

        return response()->view('tasks::list', ['tasks' => $tasks, 'search_text' => $search_text]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, int $id): Response
    {
        if (! preg_match('/tasks\/admin/', URL::previous())) {
            $request->session()->put('prev_page', URL::previous());
        }
        $task = class_task()::findOrFail($id);

        return response()->view('tasks::detail', ['task' => $task]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id): Response
    {
        $task = class_task()::findOrFail($id);
        $statuses = class_taskStatus()::all();

        return response()->view('tasks::edit', ['task' => $task, 'statuses' => $statuses]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, int $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'task_status_id' => 'required|exists:task_statuses,id',
            'date_due' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->route('tasks.admin.edit', [$id])->withErrors($validator)->withInput();
        }
        $task = class_task()::findOrFail($id);
        $attributes = $request->except('_token', '_method');
        $attributes['date_due'] = strtotime($attributes['date_due']);
        $task->update($attributes);

        return redirect()->route('tasks.admin.show', [$id]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, int $id): RedirectResponse
    {
        $task = class_task()::findOrFail($id);
        $task->delete();
        if ($request->session()->has('prev_page')) {
            return redirect()->to($request->session()->pull('prev_page'));
        } else {
            return redirect()->route('tasks.admin.index');
        }
    }

    public function defaultShow(Request $request, $id): Response
    {
        $task = class_task()::findOrFail($id);
        $taskStatuses = class_taskStatus()::whereNotIn('name', ['Pending', 'Autocanceled', 'Autocancelled', 'Completed'])->get();

        // set ther previous location in the session so we can redirect to it after form is submitted.
        $request->session()->put('prev_page', URL::previous());

        return response()->view('tasks::default_task_form', compact('task', 'taskStatuses'));
    }

    public function defaultUpdate(Request $request, $id): RedirectResponse
    {
        $validator = Validator::make($request->all(), [
            'task_complete' => 'required',
            'set_status' => 'required_if:task_complete,0',
            'task_status_id' => 'required_if:set_status,1',
        ]);
        if ($validator->fails()) {
            return redirect()->route('tasks.show', [$id])->withErrors($validator)->withInput();
        }
        $task = class_task()::findOrFail($id);
        if ($request->task_complete == 1) {
            $task->update([
                'task_status_id' => 2,
                'data' => $request->all(),
            ]);
        } else {
            if ($request->set_status == 1) {
                $task->update(['task_status_id' => $request->task_status_id]);
            } else {
                $task->date_started = new Carbon();
                $task->save();
            }
        }

        $prev_page = $request->session()->pull('prev_page', '/');

        return redirect()->to($prev_page);
    }
}
