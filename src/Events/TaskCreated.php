<?php

namespace Sirs\Tasks\Events;

use Illuminate\Queue\SerializesModels;
use Sirs\Tasks\Interfaces\Task;

class TaskCreated extends TaskEvent
{
  use SerializesModels;

  public $task;

  /**
   * Create a new event instance.
   *
   * @param  Task  $task task that status was updated on
   * @return void
   */
  public function __construct(Task $task)
  {
    $this->task = $task;
  }
}
