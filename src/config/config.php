<?php 

use Sirs\Tasks\Task;

return [
    'types' => [
        'type-1' => [
            'id' => 1,
            'name' => 'Type 1',
            // any other attributes
        ],
    ],
    'apiPrefix' => 'api',
    'routeGroup' => [
        'middleware' => ['auth', 'tasks.redirect'],
    ],
    'owner_resources' => [
        // 'Owner Class' => 'Owner Class Resource'
    ],
    'global_scopes' => [
        // App\Scopes\GlobalTaskScope::class
    ],
    'taskToAction' => [
    /** examples:
     * 'task-slug' => 'action_type:slugOrIdentifier'
     * 'survey-task' => 'survey:surveySlug',
     * 'schedule-appt-task' => 'appointment:apptTypeId',
     * 'redirect-to-url' => 'url:/path/to/thing'
     * 'custom-task' => 'custome:someIdentifier'
     **/
    ],
    'router' => App\TaskRouter::class,
    'ui' => [
        'chrome' => 'app',
    ],
    //Override default package models here
    // 'bindings' => [
    //   'models' => [
    //     'Task' => \App\Task::class,
    //     'TaskType' => \App\TaskType::class,
    //     'TaskStatus' => \App\TaskStatus::class,
    //   ]
    // ],
];
