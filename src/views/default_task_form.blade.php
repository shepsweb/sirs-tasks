@extends(config('tasks.ui.chrome'))

@section('content')
<form method="POST" name="default-task-form">
    @csrf
    @method('PUT')

    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>{{$task->owner->fullName()}} - {{$task->taskType->name}}</h4>
        </div>
        <div class="panel-body">
            <div class="form-group question-block">
                <div class="question-text">
                    {{$message ?? 'Is this task complete?'}}
                </div>
                <div class="row">
                    <div class="question-answers col-sm-9">
                        <div class="btn-group" role="group" data-toggle="buttons">
                            <label class="btn btn-default 
                                {{(old('task_complete') == '0' ) ? 'active' : '' }}">
                                <input type="radio" name="task_complete" id="task-complete-no" value="0" {{(old('task_complete') == '0' ) ? 'checked ' : '' }} data-skiptarget="set-status"/>
                                No
                           </label>
                           <label class="btn btn-default {{(old('task_complete') == '1' ) ? 'active' : '' }}">
                                <input type="radio" name="task_complete" id="task-complete-yes" value="1" {{(old('task_complete') == '1' ) ? 'checked ' : ''}}/>
                                Yes
                           </label>
                       </div>
                   </div>
                   <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('task_complete') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('task_complete') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="form-group question-block" id="set-status">
            <div class="question-text">
                Would you like to set a status for this task?
            </div>
            <div class="row">
                <div class="question-answers col-sm-9">
                    <div class="btn-group" role="group" data-toggle="buttons">
                        <label class="btn btn-default {{(old('set_status') == '0' ) ? 'active' : ''}}">
                            <input type="radio" name="set_status" id="set-status-no" value="0" {{(old('set_status') == '0' ) ? 'checked ' : ''}}/>
                            No, I'll try again later
                        </label>
                        <label class="btn btn-default {{(old('set_status') == '1' ) ? 'active' : ''}}">
                            <input type="radio" name="set_status" id="set-status-yes" value="1" {{(old('set_status') == '1' ) ? 'checked ' : ''}} data-skiptarget="task-status" />
                            Yes, set a status and finalize.
                        </label>
                    </div>
                </div>
                <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('set_status') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('set_status') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>   
        <div class="form-group question-block" id="task-status">
            <div class="question-text">
                Status
            </div>
            <div class="row">
                <div class="question-answers col-sm-9">
                    <select name="task_status_id" class="form-control">
                        <option value="">Select...</option>
                        @foreach($taskStatuses as $status)
                            <option value="{{$status->id}}"{{(old('task_status_id') == $status->id) ? ' selected':''}}>{{$status->name}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-sm-3">
                    @if ( isset($errors) && $errors->has('task_status_id') )
                    <div class="error-block">
                        <ul class="error-list list-unstyled">
                            @foreach( $errors->get('task_status_id') as $error )
                            <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                </div>
            </div>
        </div>   
    </div>
    <div class="panel-footer">
        <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
        <input class="btn btn-primary" 
            type="submit" 
            name="nav" 
            value="Finish &amp; Finalize" 
            id="finalize-btn" 
        />
    </div>
</div>
</form>
@endsection