<?php 
namespace Sirs\Tasks;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Event;
use Sirs\Tasks\Events\TaskStarted;
use Sirs\Tasks\Exceptions\InvalidOwnerId;
use Sirs\Tasks\Exceptions\InvalidOwnerType;
use Sirs\Tasks\Interfaces\Task as TaskInterface;

class Task extends Model implements TaskInterface
{
    use \Venturecraft\Revisionable\RevisionableTrait;
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'task_type_id',
        'owner_type',
        'owner_id',
        'data',
        'parent_task_id',
        'triggered_by_task_id',
        'date_due',
        'task_status_id',
        'current_survey_step',
    ];

    protected $with = [
        'taskType',
        'taskStatus',
    ];

    /**
     * The "booting" method of the model.
     */
    protected static function boot(): void
    {
        parent::boot();

        if (config('tasks.global_scopes') && count(config('tasks.global_scopes')) > 0) {
            foreach (config('tasks.global_scopes') as $scopeClassName) {
                static::addGlobalScope(new $scopeClassName);
            }
        }
    }

    protected function casts(): array
    {
        return [
            'data' => 'array',
            'date_started' => 'datetime',
            'date_due' => 'datetime',
            'created_at' => 'datetime',
            'updated_at' => 'datetime',
            'deleted_at' => 'datetime',
        ];
    }

    // Relations

    /**
     * creates polymorphic relationship with owner
     *
     **/
    public function owner(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * gets child tasks
     *
     *
     * @author tj ward
     **/
    public function children(): HasMany
    {
        return $this->hasMany(class_task(), 'parent_task_id', 'id');
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(class_task(), 'parent_task_id', 'id');
    }

    public function taskType(): BelongsTo
    {
        return $this->belongsTo(class_taskType());
    }

    public function taskStatus(): BelongsTo
    {
        return $this->belongsTo(class_taskStatus());
    }

    public function updatedBy(): MorphTo
    {
        return $this->morphTo();
    }

    public function appointments(): BelongsToMany
    {
        return $this->belongsToMany('Sirs\Appointments\Appointment');
    }

    /**
     * checks to see if $value is in config('tasks.ownerTypes') if not found throws exception
     *
     * @param  string  $value value of owner_type
     *
     * @throws InvalidOwnerId
     * @throws InvalidOwnerType
     *
     * @author
     **/
    public function setOwnerTypeAttribute($value)
    {
        // if( !in_array($value, config('tasks.ownerTypes')) ) {
        if (! in_array(\Sirs\Tasks\Interfaces\TaskOwner::class, class_implements($value))) {
            throw new InvalidOwnerType($value);
        }
        $this->attributes['owner_type'] = $value;

        if (isset($this->attributes['owner_id'])) {
            if (count(forward_static_call([$this->attributes['owner_type'], 'find'], [$this->attributes['owner_id']])) == 0) {
                throw new InvalidOwnerId($value, $this->attributes['owner_type']);
            }
        }
    }

    /**
     * checks to see if the owner id exists in the owner_type's table
     *
     * @param  int  $value value of owner_id
     *
     * @throws InvalidOwnerId [description]
     *
     * @author
     **/
    public function setOwnerIdAttribute($value)
    {
        if (isset($this->attributes['owner_type'])) {
            if (count(forward_static_call([$this->attributes['owner_type'], 'find'], [$value])) == 0) {
                throw new InvalidOwnerId($value, $this->attributes['owner_type']);
            }
        }
        $this->attributes['owner_id'] = $value;
    }

    public function getPastDueAttribute()
    {
        return $this->date_due->lt(Carbon::create()) && $this->task_status_id == 1;
    }

    // Scopes
    public function scopeStatus($query, $status_id)
    {
        if (is_array($status_id)) {
            $query->whereIn('task_status_id', $statis_id);
        } else {
            $query->where('task_status_id', $status_id);
        }

        return $query;
    }

    public function scopePending($query)
    {
        return $query->where('task_status_id', class_taskStatus()::findByName('pending')->id);
    }

    public function scopeNotChild($query)
    {
        return $query->whereNull('parent_task_id');
    }

    /**
     * undocumented function
     *
     *
     * @author
     **/
    public function scopeType($query, $typeIdOrSlug)
    {
        if (is_int($typeIdOrSlug)) {
            $typeId = $typeIdOrSlug;
        } elseif (is_string($typeIdOrSlug)) {
            if (array_key_exists($typeIdOrSlug, config('tasks.types'))) {
                $typeId = config('tasks.types')[$typeIdOrSlug];
            } else {
                throw new Exception('slug '.$typeIdOrSlug.' was not found in tasks.taskTypes');
            }
        } elseif ($typeIdOrSlug::class == \Sirs\Tasks\TaskStatus::class) {
            $typeId = $typeIdOrSlug->id;
        } else {
            throw new \Exception('Integer (id), string (slug) expected, or TaskStatus object. '.gettype($typeIdOrSlug).'found');
        }

        return $query->where('task_type_id', '=', $typeId);
    }

    // Domain methods

    /**
     * Adds the current date as start_date to the task and saves it
     *
     *
     * @author tj ward
     **/
    public function startTask(Carbon $date_started = null)
    {
        if (is_null($this->date_started)) {
            $this->date_started = ($date_started) ? $date_started : new Carbon();
            $this->current_survey_step = 1;
            $this->save();

            // Event::dispatch(new TaskStarted($this));
        }
    }

    public function completeTask(Carbon $dateCompleted = null)
    {
        $this->task_status_id = 2;
        $this->save();
    }

    /**
     * Adds the current date as start_date to the task and saves it
     *
     *
     * @author tj ward
     **/
    public function saveData($data)
    {
        $this->data = $data;
        $this->save();
    }

    public function getNameAttribute()
    {
        return $this->taskType->name;
    }

    public function getOldestAncestor()
    {
        if ($this->hasParent()) {
            return $this->parent->getOldestAncestor();
        } else {
            return $this;
        }
    }

    public function hasParent()
    {
        return $this->parent;
    }

    public function siblingsQuery()
    {
        return static::where('parent_task_id', '=', $this->parent_task_id)->where('tasks.id', '!=', $this->id)->select('tasks.*');
    }

    public function getSiblings()
    {
        if ($this->parent_task_id) {
            return $this->siblingsQuery()
                ->join('task_types', 'tasks.task_type_id', '=', 'task_types.id')
                ->orderBy('task_types.child_order')
                ->get();
        }

        return collect();
    }
}
